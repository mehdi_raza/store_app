var gcm = require('node-gcm');

var message = new gcm.Message({
    collapseKey: 'testing',
    delayWhileIdle: true,
    timeToLive: 3,
    //notification: {
    //    title: "Abode",
    //    body: "This is a notification that will be displayed ASAP."
    //},
    data: {
        title: "Abode",
        body: "This is a notification that will be displayed ASAP.",
        image: 'icon'
    }
});

var regIds = [
    "e5xkU7WMoc4:APA91bFBRT0SUptF4zw-RICdIzDXLObiyhw-rfdlw0zNTvE1itGFyWDNgEXRvU13rRZ8q9LCJnbZb5-ESHwaJ-8cEkfL9ucLoTB8jpUIXI-VFUn30fWMchLqpE2ZMZAZNc0KnSVvmwIk"
    //"dQIVshscRrs:APA91bE0QWDHheOFohrfRjHuojPHX7YccOvaUNoW_Ib3hGTUFIa2SruN8XZBUrov3sMyFvuIIzMS0ZmvHv8erYIxnqmZce0i34fNXxy6npPZJuVtzjKpRUnVXqRFm4uLWltfegU02cki"
];

// Set up the sender with you API key
var sender = new gcm.Sender('AIzaSyD_Jf8D0QGZivB9gm0R6ppRrxzdkt6_wy8');

// Now the sender can be used to send messages
sender.send(message, {registrationIds: regIds}, function (err, result) {
    if (err) console.error(err);
    else    console.log(result);
});

// Send to a topic, with no retry this time
//sender.sendNoRetry(message, {topic: '/topics/global'}, function (err, result) {
//    if (err) console.error(err);
//    else    console.log(result);
//});

'use strict';



window.addEventListener('deviceready', function () {
    console.log('hello world');
    window.localforage.defineDriver(window.cordovaSQLiteDriver).then(function() {
        return window.localforage.setDriver(window.cordovaSQLiteDriver._driver);
    },function() {

    }).then(function() {
        angular.bootstrap(document, ['app']);
    },function() {

    });
}, true);


(function () {
    angular
        .module('app', [
            'app.config',
            'app.welcome',
            /*'app.signin',
            'app.register',
            'app.setupProfile',
            'app.setupChoose',
            'app.setupGateway',
            'app.setupDevices',
            'app.setupSubscription',
            'app.setupMonitoring',
            'app.setupUser',
            'app.useCouponCode',
            'app.tabs',
            'app.alert'*/
        ])
        .run(app);


    app.$inject = [];
    function app() {

        //TODO: setup plugins
        ionic.Platform.ready(function () {
            console.log('App Started...');
            window.location.hash = 'welcome';
        });

        String.prototype.capitalize = function () {
            return this.replace(/(?:^|\s)\S/g, function (a) {
                return a.toUpperCase();
            });
        };
    }
})();

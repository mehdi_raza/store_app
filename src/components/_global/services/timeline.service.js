'use strict';

(function () {
    angular
        .module('app.config')
        .factory('timelineService', timelineService);

    timelineService.$inject = [
        'Restangular',
        'apiVersion',
        'MessageService',
        '$log',
        'associatesService',
    	'$q',
        '$state'
    ];
    function timelineService(Restangular, apiVersion,  MessageService, $log, associatesService, $q, $state) {

        var timeLinePromise;
        var alarmPromise;
        var activeAlarm;
        return {
            timeline: Restangular.all(apiVersion + 'timeline'),
            invalidate:function(){
                timeLinePromise = null;
            },
            invalidateActiveAlarm: function () {
                alarmPromise = null;
                activeAlarm = null;
            },
            timelinePageGET:function(page){
                return this.timeline.getList({page: page,size:'20'}).then(function (events) {
                    events = events.plain();
                    var timeline = {};
                    return events.length === 0 ? timeline : associatesService.associatesGET().then(function (associates) {
                        angular.forEach(events, function (event) {
                            if (event.user_id && associates && associates[event.user_id]) { //jshint ignore:line
                                event.user_name = associates[event.user_id].name;  //jshint ignore:line
                            }
                            timeline[event.id] = event;
                        });
                        return timeline;
                    }, function (error) {
                        $log.debug('Error', error.data);
                        return $q.reject(error);
                    });
                }, function (error) {
                    $log.debug('Error', error.data);
                    return $q.reject(error);
                });
            },
            timelineGET: function (force) {
                if(!timeLinePromise || force){
                    timeLinePromise = this.timelinePageGET().catch(function(error){
                        timeLinePromise = null;
                        return $q.reject(error);
                    });
                }
                return timeLinePromise;
            },
            timelineGroupTimelineGET: function (id, vTid) {
                return this.timeline.one('group_timeline?event_utc=' + id + '&verified_event_utc=' + vTid).getList().then(function (data) {
                    var array = [];
                    angular.forEach(data, function (val) {
                        array[val.id] = val;
                    });
                    return array;
                }, function (error) {
                    $log.debug('Error', error.data);
                    return $q.reject(error);
                });
            },
            timelineGroupAlarmPageGET:function(page){
                return this.timeline.one('group_alarm').getList(null, {page:page}).then(function (events) {
                    var alarms = {};
                    angular.forEach( events.plain(), function (alarm) {
                        alarms[alarm.id] = alarm;
                    });
                    return alarms;
                }, function (error) {
                    $log.debug('Error', error.data);
                    return $q.reject(error);
                });
            },
            alarmGET:function(force){
                if (!activeAlarm || force) {
                    activeAlarm = Restangular.one(apiVersion + 'alarms').get().then(function (alarm) {
                        alarm = alarm.plain();
                        return alarm;
                    }, function (error) {
                        $log.debug('Error', error.data);
                        activeAlarm = null;
                        return $q.reject(error);
                    });
                }
                return activeAlarm;
            },
            timelineGroupAlarmGET: function () {
                if(!alarmPromise){
                    alarmPromise = this.timelineGroupAlarmPageGET().catch(function(error){
                        alarmPromise = null;
			            return $q.reject(error);
                    });
                }
                return alarmPromise;
            },
            timelineIdVerifyAlarmPOST: function (id) {
                var self = this;
                return Restangular.one(apiVersion + 'timeline/' + id + '/verify_alarm').customPOST('1', null, null).then(function (data) {
                    self.invalidate();
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    MessageService.show('Please Note', {
                        content: error.data.message
                    }, [{
                        text: 'OK',
                        type: 'button-basic-blue',
                        onTap: function () {
                            if(error.data.code == 400) {    //jshint ignore:line
                                $state.go('tab.status.timeline');
                            }
                            return true;
                        }
                    }]);
                    return false;
                });
            },
            timelineIdIgnoreAlarmPOST: function (id) {
                var self = this;
                return Restangular.one(apiVersion + 'timeline/' + id + '/ignore_alarm').customPOST('1', null, null).then(function (data) {
                    self.invalidate();
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    MessageService.show('Please Note', {
                        content: error.data.message
                    }, [{
                        text: 'OK',
                        type: 'button-basic-blue',
                        onTap: function () {
                            if(error.data.code == 400) {    //jshint ignore:line
                                $state.go('tab.status.timeline');
                            }
                            return true;
                        }
                    }]);
                    return false;
                });
            }
        };
    }
})();

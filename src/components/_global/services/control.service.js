'use strict';

(function () {
    angular
        .module('app.config')
        .factory('controlService', controlService);

    controlService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        '$log',
        'MessageService'
    ];
    function controlService(Restangular, apiVersion, queryParam, $log, MessageService) { //jshint ignore:line
        return {
            controlThermostatIdPUT: function (id, options) {
                return Restangular.one(apiVersion + 'control/thermostat/' + id).put(null, queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            controlLightIdPUT: function (id, options) {
                return Restangular.one(apiVersion + 'control/light/' + id).customPUT(queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            controlPowerSwitchIdPUT: function (id, options) {
                return Restangular.one(apiVersion + 'control/power_switch/' + id).customPUT(queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    MessageService.showError(error.data.title, error.data.message);
                    return false;
                });
            },
            controlLockIdPUT: function (id, options) {
                return Restangular.one(apiVersion + 'control/lock/' + id).customPUT(queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            }
        };
    }
})();

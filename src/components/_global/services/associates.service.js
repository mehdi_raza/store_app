'use strict';

(function () {
    angular
        .module('app.config')
        .factory('associatesService', associatesService);

    associatesService.$inject = [
        'Restangular',
        'apiVersion',
        '$log',
        'queryParam',
        'MessageService',
        'commonService'
    ];
    function associatesService(Restangular, apiVersion, $log, queryParam, MessageService, commonService) { //jshint ignore:line
        var associatesPromise;
        return {
            associates: Restangular.all(apiVersion + 'associates'),
            invalidate: function () {
              associatesPromise = null;
            },
            associatesGET: function (force) {
                if (!associatesPromise || force) {
                    associatesPromise = this.associates.getList().then(function (data) {
                        var associates = {};
                        angular.forEach(data, function (val) {
                            associates[val.id] = val;
                        });
                        return associates;
                    }, function (error) {
                        $log.debug('Error', error.data);
                        associatesPromise = null;
                        return false;
                    });
                }
                return associatesPromise;
            },
            associatedUserPOST: function (options) {
                options = commonService.encodeEmail(options);
                return Restangular.one(apiVersion + 'associates/add').post(null, queryParam.value(options)).then(function (data) {
                    associatesPromise = null;
                    return data;
                }, function (error) {
                    if (error.data.code === 400) {
                        if(error.data.errors) {
                            if(error.data.errors[0]) {
                                MessageService.showError(error.data.title, error.data.errors[0].message);
                            } else {
                                MessageService.showError(error.data.title, error.data.message);
                            }
                        } else {
                            MessageService.showError(error.data.title, error.data.message);
                        }
                        return false;
                    }
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            associatedUserDELETE: function (id) {
                return Restangular.one(apiVersion + 'associates/' + id).remove().then(function () {
                }, function (error) {
                    $log.debug('Error', error.data);
                    return false;
                });
            }
        };
    }
})();

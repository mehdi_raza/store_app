'use strict';

(function () {
    angular
        .module('app.config')
        .factory('panelService', panelService);

    panelService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        '$log',
        'MessageService'
    ];
    function panelService(Restangular, apiVersion, queryParam, $log, MessageService) { //jshint ignore:line
        var panelPromise;
        return {
            panel: Restangular.service(apiVersion + 'panel'),
            invalidate: function(){
                panelPromise = null;
            },
            panelGET: function (force) {
                if(force || !panelPromise){
                    panelPromise =  this.panel.one().get().then(function (data) {
                        return data.plain();
                    }, function (error) {
                        $log.debug('Error', error.data);
                        panelPromise = null;
                        return false;
                    });
                }
                return panelPromise;
            },
            firmwareUpdate: function () {
                var me = this;
                return this.panel.one('firmware_update').put().then(function () {       // jshint ignore:line
                    return me.panelGET();
                }, function (error) {
                    $log.warn('Error', error.data);
                    MessageService.showError(error.data.title, error.data.message);
                    return false;
                });
            },
            panelPUT: function (options) {
                return this.panel.one('setup').customPUT(queryParam.value(options)).then(function (data) {
                    /*panelPromise = $q.when(data.plain());
                    return panelPromise;*/
                    panelPromise = null;
                    return data.plain();
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            panelTimeDifferenceGET: function () {
                return Restangular.one(apiVersion + 'datetime_difference').get().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error);
                    return error;
                });
            },
            panelResetTimePUT: function () {
                return Restangular.one(apiVersion + 'set_date_time').put().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            panelInitPUT: function () {
                return this.panel.one('init').put().then(function (data) {
                    if (!data.code) {
                        data.code = 200;
                    }
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    if (error) {
                        return error.data;
                    } else {
                        return false;
                    }
                });
            },
            panelActionPUT: function (action) {
                return Restangular.one(apiVersion + 'panel/learn/' + action).put().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            panelModePUT: function (changeMode) {
                var self = this;
                return Restangular.one(apiVersion + 'panel/mode/1/' + changeMode).put().then(function (data) {
                    self.invalidate();
                    return data;
                }, function (error) {
                    self.invalidate();
                    $log.warn('Error', error.data);
                    return {
                        error: error.data
                    };
                });
            },
            panelSetupZonePUT: function (options) {
                return this.panel.one('setup_zone').customPUT(queryParam.value(options)).then(function (data) {
                    /*panelPromise = $q.when(data.plain());
                    return panelPromise;*/
                    panelPromise = null;
                    return data.plain();
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            areasGET: function () {
                return Restangular.one(apiVersion + 'areas').get().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            areasPUT: function (req) {
                return Restangular.one(apiVersion + 'areas').customPUT(queryParam.value(req)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            soundsGET: function () {
                return Restangular.all(apiVersion + 'sounds').getList().then(function (data) {
                    return data;
                });
            }
        };
    }
})();

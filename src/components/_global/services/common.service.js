'use strict';

(function () {
    angular
        .module('app.config')
        .factory('commonService', commonService);

    commonService.$inject = ['$state', '$ionicHistory', 'MessageService', '$sce'];
    function commonService($state, $ionicHistory, MessageService, $sce) { //jshint ignore:line
        return {
            gotoUserScreen: function (panel) {
                if(panel.setup_users == 0) {                                            // jshint ignore:line
                    $state.go('setupUser').then(clearHistory);
                } else if (panel.setup_billing == 0) {        // jshint ignore:line
                    $state.go('setupSubscriptionIntro').then(clearHistory);
                } else  {
                    $state.go('tab.status.timeline').then(clearHistory);
                }

                function clearHistory() {
                    setTimeout(function () {
                        $ionicHistory.clearHistory();
                    }, 500);
                }
            },
            gotoSubscriptionScreen: function (panel) {
                if(panel.setup_billing == 0) {                                          // jshint ignore:line
                    $state.go('setupSubscriptionIntro');
                } else{
                    $state.go('tab.status.timeline');
                }
            },
            encodeEmail : function (options) {
                options = JSON.parse(JSON.stringify(options));
                if (options.id) {
                    options.id = options.id.replace(/\+/g, encodeURIComponent('+'));
                } else if (options.email) {
                    options.email = options.email.replace(/\+/g, encodeURIComponent('+'));
                }
                if (options.password) {
                    options.password = encodeURIComponent(options.password);
                }
                return options;
            },
            handleErrorCodes : function (error) {
                if (error.status === 504) {
                    MessageService.showError(this.errorMessages.gatewayOffline.title, this.errorMessages.gatewayOffline.message);
                } else if (error.status === 602) {
                    MessageService.showError(this.errorMessages.requestTimeout.title, this.errorMessages.requestTimeout.message);
                } else if (error.status === 601) {
                    MessageService.showError(this.errorMessages.gatewayOffline.title, this.errorMessages.gatewayOffline.message);
                } else {
                    MessageService.showError(error.data.title, error.data.message || 'Connection Error.');
                }
            },

            errorMessages : {

                requestTimeout : {

                    title: 'Connection Error',
                    message: 'We were unable to communicate with your abode gateway or it is taking too long to respond. Please ensure your gateway is connected to your router with the Ethernet cable. If the problem persists, please reboot your abode Gateway.'

                }, gatewayTimeout : {

                    title: 'Connection Error',
                    message: 'We were unable to communicate with your abode gateway or it is taking too long to respond. Please ensure your gateway is connected to your router with the Ethernet cable. If the problem persists, please reboot your abode Gateway.'

                }, gatewayOffline : {

                    title: 'Gateway Offline',
                //    message: $sce.trustAsHtml('Your gateway is offline. Please ensure your gateway is connected to your router with the Ethernet cable. If the problem persists, please reboot your abode Gateway. When your gateway completes the reboot process, you will receive an abode Gateway Online email and then you can proceed with the set-up process. Please contact <a href="mailto:support@goabode.com">support@goabode.com</a>, if problem persists.')
                    message: 'Your gateway is offline. Please check your Internet connection.  If your Internet is working properly, please ensure your gateway is connected to your router with the Ethernet cable. If the problem persists, please reboot your abode Gateway.'

                }
            },


            isObjEquivalent : function (a, b) {
                // removing $$hashKey property added by Angular
                a = JSON.parse(angular.toJson(a));
                b = JSON.parse(angular.toJson(b));

                // Create arrays of property names
                var aProps = Object.getOwnPropertyNames(a);
                var bProps = Object.getOwnPropertyNames(b);

                // If number of properties is different,
                // objects are not equivalent
                if (aProps.length != bProps.length) {           //jshint ignore:line
                    return false;
                }

                for (var i = 0; i < aProps.length; i++) {
                    var propName = aProps[i];

                    // If values of same property are not equal,
                    // objects are not equivalent
                    // ignoring type of properties
                    if (a[propName] != b[propName]) {           //jshint ignore:line
                        return false;
                    }
                }

                // If we made it this far, objects
                // are considered equivalent
                return true;
            },
            getDeviceModel: function () {
                var deviceModel = window.device.model;
                var appleDevices = {
                    'iPhone1,1':	'iPhone',
                    'iPhone1,2':	'iPhone 3G',
                    'iPhone2,1':	'iPhone 3GS',
                    'iPhone3,1':	'iPhone 4 GSM',
                    'iPhone3,3':	'iPhone 4 CDMA',
                    'iPhone4,1':	'iPhone 4S',
                    'iPhone5,1':	'iPhone 5',
                    'iPhone5,2':	'iPhone 5',
                    'iPhone5,3':	'iPhone 5c',
                    'iPhone5,4':	'iPhone 5c',
                    'iPhone6,1':	'iPhone 5s',
                    'iPhone6,2':	'iPhone 5s',
                    'iPhone7,1':	'iPhone 6 Plus',
                    'iPhone7,2':	'iPhone 6',
                    'iPhone8,1':	'iPhone 6s',
                    'iPhone8,2':	'iPhone 6s Plus',
                    'iPhone8,4':	'iPhone SE',
                    'iPhone9,1':	'iPhone 7',
                    'iPhone9,2':	'iPhone 7 Plus',
                    'iPhone9,3':	'iPhone 7',
                    'iPhone9,4':	'iPhone 7 Plus',
                    'iPad1,1':	    'iPad',
                    'iPad2,1':	    'iPad 2 - WiFi',
                    'iPad2,2':	    'iPad 2 - GSM',
                    'iPad2,3':	    'iPad 2 - CDMA',
                    'iPad2,4':	    'iPad 2 - WiFi',
                    'iPad2,5':	    'iPad mini',
                    'iPad2,6':	    'iPad mini',
                    'iPad2,7':	    'iPad mini',
                    'iPad3,1':	    'iPad - 3rd gen',
                    'iPad3,2':	    'iPad - 3rd gen',
                    'iPad3,3':	    'iPad - 3rd gen',
                    'iPad3,4':	    'iPad - 4th gen',
                    'iPad3,5':	    'iPad - 4th gen',
                    'iPad3,6':	    'iPad - 4th gen',
                    'iPad4,1':	    'iPad Air',
                    'iPad4,2':	    'iPad Air',
                    'iPad4,3':	    'iPad Air',
                    'iPad4,4':	    'iPad mini 2',
                    'iPad4,5':	    'iPad mini 2',
                    'iPad4,6':	    'iPad mini 2',
                    'iPad4,7':	    'iPad mini 3',
                    'iPad4,8':	    'iPad mini 3',
                    'iPad4,9':	    'iPad mini 3',
                    'iPad5,1':	    'iPad mini 4',
                    'iPad5,2':	    'iPad mini 4',
                    'iPad5,3':	    'iPad Air 2',
                    'iPad5,4':	    'iPad Air 2',
                    'iPad6,3':	    'iPad Pro - 9.7 inch',
                    'iPad6,4':	    'iPad Pro - 9.7 inch',
                    'iPad6,7':	    'iPad Pro - 12.9 inch',
                    'iPad6,8':	    'iPad Pro - 12.9 inch'
                };

                if (appleDevices[deviceModel]) {
                    deviceModel = appleDevices[deviceModel];
                }

                return deviceModel;
            }
        };
    }
})();

'use strict';

(function () {
    angular
        .module('app.config')
        .factory('statesService', statesService);

    statesService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        '$log'
    ];
    function statesService(Restangular, apiVersion, $log) { //jshint ignore:line
        return {
            states: Restangular.service(apiVersion + 'states'),
            statesGET: function () {
                return this.states.one().getList().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            }
        };
    }
})();

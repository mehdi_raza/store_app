'use strict';

(function () {
    angular
        .module('app.config')
        .factory('contactsService', contactsService);

    contactsService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        '$log'
    ];
    function contactsService(Restangular, apiVersion, queryParam, $log) { //jshint ignore:line
        var contactsPromise;
        return {
            contacts: Restangular.service(apiVersion + 'contacts'),
            deletedContacts: [],
            newContacts: [],
            invalidate: function () {
                contactsPromise = null;
            },
            contactsGET: function () {
                if (!contactsPromise) {
                    contactsPromise = this.contacts.one().getList().then(function (data) {
                        return data.plain();
                    }, function (error) {
                        $log.debug('Error', error.data);
                        contactsPromise = null;
                        return false;
                    });
                }
                return contactsPromise;
            },
            contactsIdGET: function (id) {
                return Restangular.one(apiVersion + 'contacts/' + id).get().then(function (data) {
                    return data;
                }, function (error) {
                    $log.debug('Error', error.data);
                    return false;
                });
            },
            contactsPOST: function (options) {
                var me = this;
                return Restangular.one(apiVersion + 'contacts').customPOST(queryParam.value(options)).then(function (data) {
                    me.invalidate();
                    return data.plain();
                }, function (error) {
                    $log.debug('Error', error.data);
                    return false;
                });
            },
            contactsPUT: function (options) {
                var me = this;
                return Restangular.one(apiVersion + 'contacts/' + options.id).customPUT(queryParam.value(options)).then(function (data) {
                    me.invalidate();
                    return data;
                }, function (error) {
                    //MessageService.showError(error.data.title, error.data.error);
                    $log.debug('Error', error.data);
                    return false;
                });
            },
            contactsDELETE: function (id) {
                var me = this;
                return Restangular.one(apiVersion + 'contacts/' + id).remove().then(function () {
                    me.invalidate();
                }, function (error) {
                    $log.debug('Error', error.data);
                    return false;
                });
            }
        };
    }
})();

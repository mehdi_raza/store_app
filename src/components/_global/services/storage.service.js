'use strict';

(function () {
    angular
        .module('app.config')
        .factory('storageService', storageService);

    storageService.$inject = [ '$localForage','$q', '$rootScope'];
    function storageService($localForage,$q, $rootScope) {
        return {
            getStack:function(){
               return window.cleanStack(new Error('dummy').stack.replace(/.*\n/,''));//remove first entry
            },
            getAsync: function (itemOrArray) {
                var stack = this.getStack();
                return $localForage.getItem(itemOrArray).catch(function(ex){
                    window.log ({
                        ERROR:ex.message,
                        Key:itemOrArray,
                        Stack:stack,
                    });
                    return $q.reject(ex);
                });
            },

            setAsync: function (itemOrArray, value) {
                /*
                 * @ANDROID: this is done to remove null, undefined, and functions from
                 * the objects, which causes android app to through exception because
                 * Restangular attaches all its properties to the object/array returned
                 * by any API call.
                 */
                var cleanValue = JSON.parse(JSON.stringify(value));
                var stack = this.getStack();
                return $localForage.setItem(itemOrArray, cleanValue).catch(function(ex){
                    window.log ({
                            ERROR: ex.message,
                            Key: itemOrArray,
                            Stack: stack,
                        });
                    return $q.reject(ex);
                });
            },

            removeAsync: function (itemOrArray) {
                var stack = this.getStack();
                return $localForage.removeItem(itemOrArray).catch(function(ex){
                    window.log ({
                        ERROR:ex.message,
                        Key:itemOrArray,
                        Stack:stack,
                    });
                    return $q.reject(ex);
                });
            },

            userDefaultsSetItem: function (key, value, complete) {
                try {
                    var suitePrefs = plugins.appPreferences.suite('group.sharedAppTokenResource');  //jshint ignore:line
                    suitePrefs.store(function (value) {
                        console.log('Storing value in User Defaults: ', value);
                        complete(true);
                    }, function (err) {
                        window.log({
                            Action: 'Setting token to User Defaults',
                            Error: err,
                            Background: $rootScope.paused === true
                        });
                        complete(false);
                    }, key, value);
                } catch (ex) {
                    console.log(ex);
                }
            },

            userDefaultsGetItem: function (key, complete) {
                try {
                    var suitePrefs = plugins.appPreferences.suite('group.sharedAppTokenResource');  //jshint ignore:line
                    suitePrefs.fetch(function (success) {
                        console.log('Success: ', success);
                        complete(success);
                    }, function (err) {
                        window.log({
                            Action: 'Getting token from User Defaults',
                            Error: err,
                            Background: $rootScope.paused === true
                        });
                        complete(null);
                    }, key);
                } catch (ex) {
                    console.log(ex);
                }
            },

            userDefaultsRemoveItem: function (key, complete) {
                try {
                    var suitePrefs = plugins.appPreferences.suite('group.sharedAppTokenResource');  //jshint ignore:line
                    this.userDefaultsGetItem(key, function (data) {
                        if (data) {
                            suitePrefs.remove(function () {
                                complete(true);
                                console.log('Key: ' + key + ' removed successfully...');
                            }, function (err) {
                                complete(false);
                                window.log({
                                    Action: 'Removing ' + key + ' from User Defaults',
                                    Error: err,
                                    Background: $rootScope.paused === true
                                });
                            }, key);
                        }
                    });
                } catch (ex) {
                    console.log(ex);
                }
            }


        };
    }
})();

'use strict';

(function () {
    angular
        .module('app.config')
        .factory('userService', userService);

    userService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        'MessageService',
        '$log',
        'storageService'
    ];
    function userService(Restangular, apiVersion, queryParam, MessageService, $log, storageService) {
        var userPromise;
        return {
            user: Restangular.service(apiVersion + 'user'),
            cms_code: null,     // jshint ignore:line
            invalidate: function () {
                userPromise = null;
            },
            removeToken:function(){
                return storageService.removeAsync('token');
            },
            getToken: function () {
                return storageService.getAsync('token');
            },
            userGET: function (force) {
                if (!userPromise || force) {
                    userPromise = this.user.one().get().then(function (data) {
                        return data.plain();
                    }, function (error) {
                        $log.debug('Error', error.data);
                        userPromise = null;
                        return false;
                    });
                }
                return userPromise;
            },
            userPOST: function (options, url, hidePopups) {
                if (!hidePopups) {
                    MessageService.showLoading('Saving...');
                }
                return this.user.one(url).post(null, queryParam.value(options)).then(function (data) {
                    if (data.plain) {
                        data = data.plain();
                    }
                    if (!hidePopups) {
                        MessageService.hideLoading();
                    }
                    userPromise = null;
                    return data;
                }, function (error) {
                    if (!hidePopups) {
                        MessageService.hideLoading();
                        MessageService.showError(error.data.title, error.data.errors ? error.data.errors[0].message : error.data.message);
                    }
                    $log.warn('Error', error.data);
                    return {
                        error: error.data
                    };
                });
            },
            userChangePasswordPUT: function (options) {
                return Restangular.one(apiVersion + 'change_password').put(null, queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            appVersionGET: function () {
                return Restangular.one('auth2/version').get().then(function (data) {
                    return data;
                });
            },
            contactsGET: function () {
                //contacts
                return Restangular.one(apiVersion + 'contacts').get().then(function (data) {
                    return data;
                });
            },
            tosGET: function () {
                return Restangular.one('registration/tos').get().then(function (data) {
                    return data;
                });
            },
            tosPOST: function () {
                return this.user.post(queryParam.value({
                    agreement: '1'
                })).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return {
                        error: error.data
                    };
                });
            }
        };
    }
})();

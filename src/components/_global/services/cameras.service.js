'use strict';

(function () {
    angular
        .module('app.config')
        .factory('camerasService', camerasService);

    camerasService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        'MessageService',
        '$log',
        '$q',
        'commonService'
    ];
    function camerasService(Restangular, apiVersion, queryParam, MessageService, $log,  $q, commonService) { //jshint ignore:line
        var camerasPromise = null;
        var camerasPromises = {};
        return {
            cameras: Restangular.service(apiVersion + 'devices'),
            filterCameras : function (data) {
                var arr = [];
                angular.forEach(data, function(val){
                    if(val['type_tag'] === 'device_type.ipcam' || val['type_tag'] === 'device_type.ir_camcoder' || val['type_tag'] === 'device_type.ir_camera' || val['type_tag'] === 'device_type.out_view' || val['type_tag'] === 'device_type.nest_cam') {  //jshint ignore:line
                        arr.push(val);
                    }
                });
                return arr;
            },
            invalidate: function(){
                camerasPromise = null;
                camerasPromises = {};
            },
            camerasGET: function () {
                var me = this;
                if(!camerasPromise){
                    camerasPromise = this.cameras.one().getList().then(function (cameras) {
                        var cameraData = {};
                        var promises = [];
                        angular.forEach(me.filterCameras(cameras.plain()), function (val) {
                            cameraData[val.id] = val;
                            promises.push(Restangular.one(apiVersion + 'timeline?device_id=' + val.id).get().then(function (captures) {  //jshint ignore:line
                                cameraData[val.id].files = captures;
                                return captures;
                            }, function (error) {
                                $log.debug('Error', error.data);
                                camerasPromise = null;//let's not cache since we got an error
                                return false;
                            }));
                        });

                        return $q.all(promises).then(function(){
                            return cameraData;
                        });
                    }, function (error) {
                        $log.debug('Error', error.data);
                        camerasPromise = null;
                        return false;
                    });
                }
                return camerasPromise;
            },
            camerasFilesGET: function (id) {
                if(!camerasPromises[id]){
                    camerasPromises[id] = this.camerasGET().then(function(cameras){
                        return Restangular.one(apiVersion + 'timeline?device_id=' + id).get().then(function (data) {
                            angular.forEach(data, function (item) {
                                if(item.file_count === '3') {                                       // jshint ignore:line
                                    for (var i = 2; i<=3; i++){
                                        var obj = angular.copy(item);
                                        obj.file_path = item.file_path.replace('001', '00' + i);    // jshint ignore:line
                                        data.push(obj);
                                    }
                                }
                            });

                            if(cameras[id] && cameras[id].files) {
                                cameras[id].files = data.plain();
                            }
                            return cameras[id];
                        }, function (error) {
                            $log.debug('Error', error.data);
                            camerasPromises[id] = null;
                            return null;
                        });
                    }, function (error) {
                        $log.debug('Error', error.data);
                        camerasPromises[id] = null;
                        return null;
                    });
                }
                return camerasPromises[id];
            },
            camerasHistoryGET: function (url) {
                return Restangular.one(apiVersion + 'storage?url=' + url).get().then(function (data) {
                    return data;
                }, function (data) {
                    return data.data;
                });
            },
            camerasActivateGET: function (code) {
                MessageService.showLoading('Activating...');
                return Restangular.one(apiVersion + 'cams/activate?code=' + code).get().then(function (data) {
                    MessageService.hideLoading();
                    return data;
                }, function (error) {
                    $log.debug('Error', error.data);
                    MessageService.hideLoading();
                    MessageService.showError(error.data.title, error.data.message);
                    return false;
                });
            },
            camerasWifiGET: function (mac) {
                return Restangular.one(apiVersion + 'cams/wifi?mac=' + mac).getList().then(function (data) {
                    return data;
                }, function (error) {
                    $log.debug('Error', error.data);
                    commonService.handleErrorCodes(error);
                    return false;
                });
            },
            camerasIdRecordPUT: function (url) {
                var self = this;
                MessageService.showLoading('Recording Video...');
                return Restangular.one(apiVersion + url).put().then(function (data) {
                    MessageService.showSuccess('Video Successful');
                    self.invalidate();
                    return data;
                }, function (error) {
                    MessageService.hideLoading();
                    $log.warn('Error', error.data);
                    commonService.handleErrorCodes(error);
                    return false;
                });
            },
            camerasTestSnapOnSetupPUT : function(id){
                return Restangular.one(apiVersion + 'cams/' + id + '/capture').put().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    commonService.handleErrorCodes(error);
                    return false;
                });
            },
            camerasIdCapturePUT: function (url) {
                var self = this;
                MessageService.showLoading('Taking Picture...');
                return Restangular.one(apiVersion + url).put().then(function (data) {
                    MessageService.showSuccess('Image Successful');
                    self.invalidate();
                    return data;
                }, function (error) {
                    MessageService.hideLoading();
                    $log.warn('Error', error.data);
                    commonService.handleErrorCodes(error);
                    return false;
                });
            },
            camerasStreamSessionPOST: function (options) {
                return Restangular.one(apiVersion + 'stream/session').post(null, queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            camerasStreamStartPOST: function (options) {
                return Restangular.one(apiVersion + 'stream/start').post(null, queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            camerasWifiPUT: function (data, mac) {
                if (data.password) {
                    MessageService.showLoading('Joining Network...');
                    var form = {
                        mac: mac,
                        enable: 1,
                        ssid: data.ssid,
                        auth: data.auth,
                        encrypt: data.encrypt,
                        password: data.password
                    };
                    return Restangular.one(apiVersion + 'cams/wifi').customPUT(queryParam.value(form)).then(function (data) {
                        MessageService.hideLoading();
                        return data;
                    }, function (error) {
                        MessageService.hideLoading();
                        commonService.handleErrorCodes(error);
                        return false;
                    });
                } else {
                    MessageService.showError(undefined, 'Enter WiFi password.');
                }
            }
        };
    }
})();

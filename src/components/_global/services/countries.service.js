'use strict';

(function () {
    angular
        .module('app.config')
        .factory('countriesService', countriesService);

    countriesService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        '$log'
    ];
    function countriesService(Restangular, apiVersion, $log) { //jshint ignore:line
        return {
            countries: Restangular.service(apiVersion + 'countries'),
            countriesGET: function () {
                return this.countries.one().getList().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            }
        };
    }
})();

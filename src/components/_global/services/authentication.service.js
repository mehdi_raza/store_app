'use strict';

(function () {
    angular
        .module('app.config')
        .factory('authenticationService', authenticationService);

    authenticationService.$inject = [
        'Restangular',
        'queryParam',
        'MessageService',
        'apiVersion',
        '$rootScope',
        '$ionicHistory',
        'storageService',
        'commonService',
        'userService'
    ];
    function authenticationService(Restangular, queryParam, MessageService, apiVersion, $rootScope, $ionicHistory, storageService,  commonService, userService) {

        function setOptions(token, options) {
            if (window.cordova && token) {
                options.device_model = commonService.getDeviceModel();                 //jshint ignore:line
                if (window.device.platform === 'iOS') {
                    options.apns_token = token;                             //jshint ignore:line
                } else {
                    options.gcm_token = token;                              //jshint ignore:line
                }
                options.app_version = $rootScope.vm.appVersion;             // jshint ignore:line
                options.os_version = window.device.version;                 // jshint ignore:line

                storageService.setAsync('app-version', $rootScope.vm.appVersion);
                storageService.setAsync('os-version', window.device.version);

            } else {
                options.device_model = 'iPhone NaN';                        //jshint ignore:line
                options.device_name = 'Virtual Device';                     //jshint ignore:line
                options.apns_token = 'cb7235d5f91cb09f99e0328ee952fe76806af9f6d334a1a65f16d329c56785ce'; //jshint ignore:line
            }

            return options;
        }

        return {
            loginPOST: function (options) {
                return storageService.getAsync('pushToken').then(function (token) {
                    options = setOptions(token, options);

                    MessageService.showLoading('Logging In...');
                    options = commonService.encodeEmail(options);
                    return Restangular.one('auth2/login').post(null, queryParam.value(options)).then(function (data) {
                        return userLogged(data).then(function () {
                            return data;
                        });
                    }, function (error) {
                        MessageService.hideLoading();
                        if (error.data) {
                            if (error.data.errors) {
                                MessageService.showError(error.data.title, error.data.errors[0].message);
                                return false;
                            } else if (error.data.message.indexOf('verify') > -1) {
                                return {error: 'validation', message: error.data.message};
                            } else {
                                MessageService.showError(error.data.title, error.data.message);
                                return false;
                            }
                        }
                    });
                });
            },
            sessionGET: function () {
                return Restangular.one(apiVersion + 'session').get().then(function (data) {
                    data = data.plain();
                    return storageService.setAsync('session', data).then(function () {
                        return data;
                    });
                });
            },
            loginCheck: function (options) {
                MessageService.showLoading('Logging In...');
                return Restangular.one('auth2/login').post(null, queryParam.value(options)).then(function (data) {
                    MessageService.hideLoading();
                    return data;
                }, function (error) {
                    MessageService.hideLoading();
                    if (error.data) {
                        if (error.data.errors) {
                            MessageService.showError(error.data.title, error.data.errors[0].message);
                            return false;
                        } else if (error.data.message.indexOf('verify') > -1) {
                            return {error: 'validation', message: error.data.message};
                        } else {
                            MessageService.showError(error.data.title, error.data.message);
                            return false;
                        }
                    }
                });
            },
            verifyPOST: function (options) {
                return storageService.getAsync('pushToken').then(function (token) {
                    options = setOptions(token, options);

                    MessageService.showLoading('Verifying...');
                    return Restangular.one('reg/verify').post(null, queryParam.value(options)).then(function (data) {
                        return userLogged(data).then(function () {
                            return data;
                        });
                    }, function (error) {
                        MessageService.hideLoading();
                        if (error.data) {
                            if (error.data.errors) {
                                MessageService.showError(error.data.title, error.data.errors[0].message);
                            } else {
                                MessageService.showError(error.data.title, error.data.message);
                            }
                        }
                        return false;
                    });
                });
            },
            userProfileGET: function (authCode) {
                return Restangular.one('reg/user_profile/' + authCode).get().then(function (data) {
                    return data;
                }, function (error) {
                    if (error.data.errors) {
                        MessageService.showError(error.data.title, error.data.errors[0].message);
                    } else {
                        MessageService.showError(error.data.title, error.data.message);
                    }
                    return false;
                });
            },
            userProfilePOST: function (options) {
                return Restangular.one('reg/user_profile').post(null, queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    MessageService.showError(error.data.title, error.data.errors[0].message);
                    return false;
                });
            },
            registerPOST: function (options) {
                options = commonService.encodeEmail(options);
                MessageService.showLoading('Registering...');
                return Restangular.one('reg/new').post(null, queryParam.value(options)).then(function (data) {
                    MessageService.hideLoading();
                    return data;
                }, function (error) {
                    MessageService.hideLoading();
                    MessageService.showError(error.data.title, error.data.errors[0].message);
                    return false;
                });
            },
            registerResendPOST: function (email) {
                MessageService.showLoading('Sending...');
                return Restangular.one('reg/resend').post(null, queryParam.value({id: email})).then(function (data) {
                    MessageService.hideLoading();
                    return data;
                }, function (error) {
                    MessageService.hideLoading();
                    MessageService.showError(error.data.title, error.data.errors[0].message);
                    return false;
                });
            },
            forgotPassword: function (options) {
                MessageService.showLoading('Sending...');
                return Restangular.one('reg/forget_password').post(null, queryParam.value(options)).then(function (data) {
                    MessageService.hideLoading();
                    return data;
                }, function (error) {
                    MessageService.hideLoading();
                    MessageService.showError(error.data.title, error.data.message);
                });
            },
            resetPassword: function (options) {
                options = commonService.encodeEmail(options);
                MessageService.showLoading('Updating password...');
                return Restangular.one('reg/forgetpassword_change').post(null, queryParam.value(options)).then(function (data) {
                    MessageService.hideLoading();
                    return data;
                }, function (error) {
                    MessageService.hideLoading();

                    if(!error.data.errors) {
                        MessageService.showError(error.data.title, error.data.message);
                    } else {
                        MessageService.showError(error.data.title, error.data.errors[0].message);
                    }
                    return false;
                });
            },
            logOutPOST: function () {
                return Restangular.one(apiVersion + 'logout').customPOST('1', null, null).then(function () {
                    userService.invalidate();
                    if (window.device.platform === 'iOS') {
                        storageService.userDefaultsRemoveItem('token', function (resp) {
                            if (resp) {
                                console.log('Token Removed from User Defaults...');
                            }
                        });
                    }
                   return userService.removeToken();
                });
            },
            notificationPUT: function (options) {
                return Restangular.one(apiVersion + 'push_notification').customPUT(queryParam.value(options)).then(function (data) {    //jshint ignore:line

                }, function (error) {   //jshint ignore:line

                });
            }
        };

        function userLogged(data) {

            userService.invalidate();
            return userService.removeToken().then(function () {
                $ionicHistory.clearCache();
                MessageService.hideLoading();

                Restangular.setDefaultHeaders({
                    'ABODE-API-KEY': data.token,
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Access-Control-Allow-Credentials': true
                });


                if (window.device.platform === 'iOS') {
                    storageService.userDefaultsSetItem('token', data.token, function (resp) {
                        if (resp) {
                            storageService.userDefaultsGetItem('token', function (token) {
                                if (token) {
                                    console.log(token);
                                } else {
                                    console.log('Token doesn\'t exist...');
                                }
                            });
                        }
                    });
                }

                return storageService.setAsync('token', {
                    expired_at: data.expired_at, //jshint ignore:line
                    token: data.token
                });
            });
        }
    }
})();

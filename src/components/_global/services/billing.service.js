'use strict';

(function () {
    angular
        .module('app.config')
        .factory('billingService', billingService);

    billingService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        'MessageService',
        '$log',
        'userService'
    ];
    function billingService(Restangular, apiVersion, queryParam, MessageService, $log, userService) { //jshint ignore:line
        var plansPromise, billingPromise = false;
        return {
            billing: Restangular.service(apiVersion + 'billing'),
            card : {},
            invalidate: function () {
                plansPromise = null;
            },
            billingPUT: function (options) {
                if (!billingPromise) {
                    billingPromise = true;
                    if (options.id == 2 || options.id == 4 || options.id == 5 || options.id == 7) {     // jshint ignore:line
                        MessageService.showLoading('Establishing professional monitoring<br /> connection.  This process may <br />take up to 30 seconds.');
                    } else {
                        MessageService.showLoading('Processing ...');
                    }
                    return this.billing.one().customPUT(queryParam.value(options)).then(function (data) {
                        //MessageService.hideLoading();
                        billingPromise = false;
                        return data;
                    }, function (error) {
                        billingPromise = false;
                        $log.warn('Error', error.data);
                        if (!error.data.error) {
                            error.data.error = true;
                        }
                        if (error.data.code === 400) {
                            //MessageService.showError(error.data.title, error.data.message);
                            return error.data;
                        } else if (error.data.code === 412) {
                            userService.userGET().then(function (user) {
                                MessageService.show('Sorry ' + user.first_name,       // jshint ignore:line
                                    {
                                        content: 'You are currently subscribed to a 3 or 7 day monitoring plan, so you cannot upgrade to the Connect+Secure plan at this time. After your current monitoring plan expires, you can sign up for the Connect+Secure plan.'
                                    }, [{
                                        text: 'OK',
                                        type: 'button-basic-blue'
                                    }]
                                );
                            });
                        } else {
                            return {
                                error: true
                            };
                        }
                    });
                }
            },
            billingCardGET: function () {
                var self = this;
                return Restangular.one(apiVersion + 'billing/card').get().then(function (data) {
                    self.card = data;
                    return data;
                }, function (error) {
                    $log.debug('Error', error.data);
                    return false;
                });
            },
            plansGET: function () {
                if (!plansPromise) {
                    plansPromise = Restangular.one(apiVersion + 'plans?features=1').getList().then(function (data) {
                        var plans = {};
                        data = data.plain();
                        angular.forEach(data, function (val) {
                            plans[val.id] = val;
                        });
                        return plans;
                    }, function (error) {
                        $log.debug('Error', error.data);
                        plansPromise = null;
                        return false;
                    });
                }
                return plansPromise;
            }
        };
    }
})();

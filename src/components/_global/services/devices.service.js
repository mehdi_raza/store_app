'use strict';

(function () {
    angular
        .module('app.config')
        .factory('devicesService', devicesService);

    devicesService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        '$log',
        '$q',
        'MessageService',
        'commonService'
    ];
    function devicesService(Restangular, apiVersion, queryParam, $log, $q, MessageService, commonService) {
        var devicesPromise;
        return {
            devices: Restangular.service(apiVersion + 'devices'),
            invalidate: function () {
                devicesPromise = null;
            },
            devicesGET: function (force) {
                if (!devicesPromise || force) {
                    devicesPromise = this.devices.one().getList().then(function (data) {
                        data = data.plain();
                        var deviceList = {};
                        angular.forEach(data, function (val) {
                            if (val.status === 'On' || val.status === 'LockClosed' || val.status === 'Closed') {
                                val.toggleModel = true;
                            }
                            deviceList[val.id] = val;
                        });
                        return deviceList;
                    }, function (error) {
                        $log.debug('Error', error.data);
                        devicesPromise = null;
                        return false;
                    });
                }
                return devicesPromise;
            },
            devicesIdGET: function (id) {
                return this.devices.one(id).getList().then(function (data) {
                    var devicesId = {};
                    angular.forEach(data, function (val) {
                        devicesId[val.id] = angular.copy(val);
                    });
                    return devicesId;
                }, function (error) {
                    $log.debug('Error', error.data);
                    return false;
                });
            },
            devicesNewGET: function () {
                return Restangular.one(apiVersion + 'devices/new').getList().then(function (data) {
                    return data;
                }, function (error) {
                    $log.debug('Error', error.data);
                    return false;
                });
            },
            devicesPOST: function (id, options) {
                return Restangular.one(apiVersion + 'devices/' + id).post(null, queryParam.value(options)).then(function (data) {
                    /*devicesPromise = $q.when(data.plain());
                    return devicesPromise;*/
                    devicesPromise = null;
                    return data.plain();
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            devicesPUT: function (id, options) {
                return Restangular.one(apiVersion + 'devices/' + id).customPUT(queryParam.value(options)).then(function (data) {
                    /*devicesPromise = $q.when(data.plain());
                    return devicesPromise;*/
                    devicesPromise = null;
                    return data.plain();
                }, function (error) {
                    $log.warn('Error', error.data);
                    commonService.handleErrorCodes(error);
                    return false;
                });
            },
            dimmerControlPUT: function (url, options) {
                return Restangular.one(url.replace('api/', '')).customPUT(queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    commonService.handleErrorCodes(error);
                    return false;
                });
            }
        };
    }
})();

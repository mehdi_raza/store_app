'use strict';

(function () {
    angular
        .module('app.config')
        .factory('automationService', automationService);

    automationService.$inject = [
        'Restangular',
        'apiVersion',
        'queryParam',
        '$log',
        '$timeout',
        'api',
        '$q',
        'devicesService',
        'userService',
        '$rootScope',
        'storageService',
        'localLogService'
    ];
    function automationService(Restangular, apiVersion, queryParam, $log, $timeout, api, $q, devicesService, userService, $rootScope, storageService, localLogService) { //jshint ignore:line
        var automationPromise;
        var locationsPromise;
        var syncRepeat = 1;
        var service = {
            automation: Restangular.service(apiVersion + 'automation'),
            invalidate: function () {
                automationPromise = null;
                locationsPromise = null;
            },
            automationGET: function () {
                if (!automationPromise) {
                    var me = this,
                        regexp = new RegExp('.*?;', 'g');

                    automationPromise = devicesService.devicesGET().then(function (devices) {
                        return me.automation.one().getList().then(function (data) {
                            data = data.plain();

                            angular.forEach(data, function (val) {
                                val.devices = {};
                                var actions = val.actions.match(regexp);
                                angular.forEach(actions, function (act) {
                                    if (act) {
                                        if (act === 'a=1&m=0;') {
                                            val.devices.mode = {
                                                name: 'Standby',
                                                id: 0,
                                                label: 'Change System Mode to Standby.'
                                            };
                                        } else if (act === 'a=1&m=1;') {
                                            val.devices.mode = {
                                                name: 'Away',
                                                id: 1,
                                                label: 'Change System Mode to Away.'
                                            };
                                        } else if (act === 'a=1&m=2;') {
                                            val.devices.mode = {
                                                name: 'Home',
                                                id: 2,
                                                label: 'Change System Mode to Home.'
                                            };
                                        } else if (act === 'a=1&req=img_all;') {
                                            val.devices.all = {
                                                name: 'Auto Flash',
                                                id: 3,
                                                label: 'Capture Images From all Motion Cameras'
                                            };
                                        } else if (act === 'a=1&req=vid_all;') {
                                            val.devices.all = {
                                                name: 'All Video',
                                                id: 4,
                                                label: 'Capture Video From all IP Cameras'
                                            };
                                        } else if (act === 'a=1&req=img_all_nf;') {
                                            val.devices.all = {
                                                name: 'Never Flash',
                                                id: 5,
                                                label: 'Capture Images From all Motion Cameras'
                                            };
                                        }
                                    }
                                });

                                angular.forEach(devices, function (d) {
                                    angular.forEach(d.actions, function (a) {
                                        if (actions) {
                                            if (actions.indexOf(a.value) !== -1) {
                                                if (a.value.indexOf('off') !== -1) {
                                                    d.label = a.label + ' ' + d.name;
                                                } else {
                                                    d.label = d.name + ' to ' + a.label;
                                                }
                                                val.devices[d.id] = angular.copy(d);
                                            }
                                        }
                                    });
                                });
                            });

                            return data;
                        }, function (error) {
                            $log.debug('Error', error.data);
                            automationPromise = null;
                            return false;
                        });
                    });
                }
                return automationPromise;
            },
            automationIdGET: function (id) {
                return Restangular.one(apiVersion + 'automation/' + id).get().then(function (data) {
                    var automationId = {};
                    angular.forEach(data, function (val) {
                        automationId[val.id] = angular.copy(val);
                    });
                    return automationId;
                }, function (error) {
                    $log.debug('Error', error.data);
                    return false;
                });
            },
            automationPOST: function () {
                return Restangular.one(apiVersion + 'automation/').post().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            locationsGET: function (force) {
                if (force || !locationsPromise) {
                    locationsPromise = Restangular.all(apiVersion + 'automation/locations').getList().then(function (data) {
                        syncRepeat = 1;
                        return data.plain();
                    }, function (error) {
                        locationsPromise = null;
                        if (error.status === 401 || error.status === 403 || syncRepeat > 4) {
                            localLogService.log({//we don't log these unless we are stopping the retries because the error interceptor will log all the retries
                                'type': 'location',
                                'status': 'Get Automation Locations Failed',
                                'tries': syncRepeat,
                                'responseCode': error.data ? error.data.code || error.status : error.status,
                                'responseMessage': error.data ? error.data.message || error.statusText : error.statusText
                            });
                            syncRepeat = 1;
                            return $q.reject(error);
                        }
                        else {
                            syncRepeat += 1;
                            return $timeout(function () {
                                return service.locationsGET(true);
                            }, Math.pow(2, syncRepeat) * 1000);
                        }
                    });
                }
                return locationsPromise;
            },
            automationIdApplyPUT: function (id) {
                return Restangular.one(apiVersion + 'automation/' + id + '/apply').put().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            automationStatusPUT: function (options) {
                return Restangular.one(apiVersion + 'automation/' + options.id + '/edit').customPUT(queryParam.value(options)).then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            },
            /*
             * @param force
             * to re-initialize plugin so that when app is upgraded, persistent notification should be visible again
             * force flag is set to "true" only when the app initializes (initState())
             */
            geofencing: function () {
                var bgGeo = window.BackgroundGeolocation;
                $q.all([
                    userService.getToken(),
                    storageService.getAsync('settings'),
                    api.url(),
                ]).then(function(values) {
                    if (values[1] && values[1].location && values[0]) {
                        bgGeo.configure({
                            debug: false,
                            desiredAccuracy: 0,
                            distanceFilter: 50,
                            stationaryRadius: 35,
                            stopOnTerminate: false,
                            foregroundService: true,
                            startOnBoot: true,
                            minimumActivityRecognitionConfidence: 55,
                            autoSync: true,
                            maxRecordsToPersist: 1,
                            url: values[2] + apiVersion + 'automation/apply_location',
                            method: 'PUT',
                            notificationIcon: 'drawable/icon_grey',
                            headers: {
                                'ABODE-API-KEY': values[0].token
                            }
                        }, function(state) {        //jshint ignore:line
                            bgGeo.start(function() {
                                localLogService.log({
                                    action: 'starting location service'
                                });
                                setupGeofences();
                                window.sqlitePlugin.unlock();
                            }, function(error) {
                                localLogService.log({
                                    action: 'starting location service failed',
                                    error: error
                                });
                            });
                        }, function(error) {
                            localLogService.log({
                                action: 'configuring location service failed',
                                error: error
                            });
                        });
                    } else {
                        service.geofencingCancel();
                    }
                }, function(error) {
                    localLogService.log({
                        action: 'failed to check location service',
                        error: error
                    });
                });
            },
            geofencingCancel: function () {
                var bgGeo = window.BackgroundGeolocation;
                bgGeo.stop(function () {
                    localLogService.log({action: 'stopped location service'});
                }, function () {
                    localLogService.log({action: 'stopped location service failed'});
                });
                bgGeo.getGeofences(function (fences) {
                    if (fences.length > 0) {
                        bgGeo.removeGeofences(function () {
                            localLogService.log({action: 'all fences removed'});
                        }, function () {
                            localLogService.log({action: 'removing fences failed'});
                        });
                    }
                }, function () {
                    localLogService.log({action: 'getting geofences failed'});
                });
            },
            automationIdEditPUT: function (id) {
                return Restangular.one(apiVersion + 'automation/' + id + '/edit').put().then(function (data) {
                    return data;
                }, function (error) {
                    $log.warn('Error', error.data);
                    return false;
                });
            }
        };

        $rootScope.$on('sockets.automations', function () {
            service.invalidate();
            service.geofencing();
        });

        return service;

        function setupGeofences(){
            service.locationsGET(true).then(function(data){
                if(data) {
                    var bgGeo = window.BackgroundGeolocation;
                    var locations = {};
                    angular.forEach(data, function (val) {//could be the same geofence used for multiple automations we just need the unique location set
                        if (val && val.loc_id) {//jshint ignore:line
                            locations[val.loc_id] = val;//jshint ignore:line
                        }
                    });
                    bgGeo.getGeofences(function (geofences) {
                        var oldFences = [];
                        angular.forEach(geofences, function (geofence) {
                            var coords = locations[geofence.identifier];
                            if (!coords) {
                                oldFences.push(geofence.identifier);
                            } else {
                                if (Math.round(coords.lat * 1000000) / 1000000 === geofence.latitude && Math.round(coords.lng * 1000000) / 1000000 === geofence.longitude) {
                                    delete locations[geofence.identifier];//same lets not re add
                                } else {
                                    oldFences.push(geofence.identifier);//not the same so lets remove and re add
                                }
                            }
                        });
                        var removes = [];
                        if (oldFences.length > 0) {
                            angular.forEach(oldFences, function (fence) {
                                var deferred = $q.defer();
                                bgGeo.removeGeofence(fence, function () {
                                    localLogService.log({action: 'Sync removed ' + fence + ' geofence'});
                                    deferred.resolve();
                                }, function () {
                                    localLogService.log({action: 'Sync Failed to remove ' + fence + ' geofence'});
                                    deferred.resolve();
                                });
                                removes.push(deferred.promise);
                            });
                        }

                        var fences = [];
                        angular.forEach(locations, function (val) {
                            fences.push({
                                identifier: val.loc_id,//jshint ignore:line
                                radius: parseInt(val.radius),
                                latitude: Math.round(val.lat * 1000000) / 1000000, //Geo latitude of geofence
                                longitude: Math.round(val.lng * 1000000) / 1000000, //Geo longitude of geofence
                                notifyOnEntry: true,//because of fi/lo, and lets just listen for all fence crossing for each unique location
                                notifyOnExit: true
                            });
                        });
                        if (fences.length > 0) {
                            $q.all(removes).then(function () {
                                bgGeo.addGeofences(fences, function () {
                                    localLogService.log({action: 'Sync Added ' + fences.length + ' geofences'});
                                }, function () {
                                    localLogService.log({action: 'Sync Failed to add geofences'});
                                });
                            });
                        }else{
                            localLogService.log({action: 'Sync Added no new geofences'});
                        }
                    }, function () {
                        localLogService.log({action: 'getting geofences failed'});
                    });
                }else{
                    localLogService.log({action: 'get locations returned nothing'});
                }
            },function(error){
                localLogService.log({action: 'get locations error',error:error});

            });
        }
    }
})();

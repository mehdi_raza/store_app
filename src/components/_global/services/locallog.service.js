'use strict';

(function () {
    angular
        .module('app.config')
        .factory('localLogService', localLogService);

    localLogService.$inject = ['$filter','storageService'];
    function localLogService($filter,storageService) {
        var pendingPromise;
        var queue = [];

        var service = {
            getAsync:function(){
                return storageService.getAsync('logs').then(function(logs){
                    return (JSON.parse(window.localStorage.getItem('logs')) || []).concat(logs || []);
                });
            },
            clearAsync:function(){
                window.localStorage.removeItem('logs');
                return storageService.removeAsync('logs');
            },
            log: function (item) {
                if(!item.Time) {
                    item.Time = $filter('formatDate2')(new Date());
                }
                return logInternal([item]);
            },
        };

        function logAll(items){
            return service.getAsync('logs').then(function(logs) {
                logs = $filter('orderBy')( (logs || []).concat(items), function(data){
                    return Date.parse(data.Time);
                }, true);
                var extra = (logs.length + 1) - 100;
                if(extra > 0) {
                    logs.splice(-extra, extra);
                }
                return storageService.setAsync('logs',logs).then(function(){
                    window.localStorage.removeItem('logs');//remove these
                });
            });
        }

        function logInternal(items) {
            if(!pendingPromise) {
                pendingPromise = logAll(items).finally(function(){
                    pendingPromise = null;
                    if (queue.length > 0) {
                        var current = queue;
                        queue = [];
                        return logInternal(current);
                    }
                });
            } else{
                Array.prototype.push.apply(queue, items);
            }
            return pendingPromise;
        }

        return service;
    }
})();

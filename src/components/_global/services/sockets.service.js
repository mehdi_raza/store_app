'use strict';

(function () {
    angular
        .module('app.config')
        .factory('socketsService', socketsService);

    socketsService.$inject = [
        'timelineService',
        'camerasService',
        'devicesService',
        'panelService',
        'userService',
        'MessageService',
        'authenticationService',
        'api',
        '$rootScope',
        '$log',
        'storageService',
        'localLogService'
    ];
    function socketsService(timelineService, camerasService, devicesService, panelService, userService, MessageService, authenticationService, api, $rootScope, $log, storageService, localLogService) {
        var socket = {
            sockets: null,
            pendingPing:null,
            isConnected: function () {
                return this.sockets && this.sockets.connected;
            },
            invalidateOnReconnect : function () {
                panelService.invalidate();
                timelineService.invalidate();
                timelineService.invalidateActiveAlarm();
                devicesService.invalidate();
            },
            connect: function () {
                var self = this;
                if (self.sockets) {
                    if (self.sockets.disconnected) {
                        self.disconnect();
                    }else{
                        return;
                    }
                }
                authenticationService.sessionGET().then(function () {
                    api.sockets().then(function (socketsUri) {
                        try {

                            self.sockets = io.connect(socketsUri, { // jshint ignore:line
                                transports: ['websocket'],
                                autoConnect: false,
                                forceNew: true,
                                reconnection: true,
                                reconnectionDelay: 2000,
                                reconnectionDelayMax: 5000,
                                timeout: 15000,
                                reconnectionAttempts: Infinity
                            });
                            self.sockets.open();

                            self.sockets.once('connect', function () {
                                socketOpened();
                                startPing();
                                self.sockets.on('connect_error', function (error) {
                                    self.invalidateOnReconnect();
                                    localLogService.log({
                                        Action: 'socket connect error',
                                        Error: error,
                                        Background: $rootScope.paused === true
                                    });
                                });
                                self.sockets.on('connect_timeout', function () {
                                    localLogService.log({
                                        Action: 'socket connect timeout',
                                        Background: $rootScope.paused === true
                                    });
                                });
                                self.sockets.on('connect', socketOpened);
                                self.sockets.on('reconnecting', socketReconnecting);
                                self.sockets.on('reconnect', function () {
                                    localLogService.log({
                                        Action: 'socket reconnect',
                                        Background: $rootScope.paused === true
                                    });
                                    MessageService.hideLoading();
                                });
                                self.sockets.on('reconnect_error', function (error) {
                                    MessageService.showLoading('Unable to acquire connection. <br /> Please try again later');
                                    self.invalidateOnReconnect();
                                    localLogService.log({
                                        Action: 'reconnect error',
                                        Error: error,
                                        Background: $rootScope.paused === true
                                    });
                                });
                                self.sockets.on('disconnect', function () {
                                    localLogService.log({
                                        Action: 'socket closed',
                                        Background: $rootScope.paused === true
                                    });
                                });
                                //com.goabode.device.add
                                //com.goabode.device.edit
                                //com.goabode.device.del

                                self.sockets.on('com.goabode.gateway.alert.verify', function () {
                                    socketMessage('gateway.alert.verify');
                                });
                                self.sockets.on('com.goabode.gateway.firmware.update', function (data) {
                                    socketMessage('gateway.firmware.update', data);
                                });
                                self.sockets.on('com.goabode.gateway.firmware', function (data) {
                                    socketMessage('gateway.firmware.success', data);
                                });
                                self.sockets.on('com.goabode.gateway.getrecord', function (data) {
                                    socketMessage('gateway.getrecord', data);
                                });
                                self.sockets.on('com.goabode.gateway.mode', function (data) {
                                    socketMessage('gateway.mode', data);
                                });
                                self.sockets.on('com.goabode.gateway.online', function (data) {
                                    socketMessage('gateway.online', data);
                                });
                                self.sockets.on('com.goabode.timeline.refresh', function (data) {
                                    socketMessage('gateway.timeline.refresh', data);
                                });
                                self.sockets.on('com.goabode.gateway.timeline', function (data) {
                                    socketMessage('gateway.timeline', data);
                                });
                                self.sockets.on('com.goabode.gateway.timeline.update', function (data) {
                                    socketMessage('gateway.timeline.update', data);
                                });
                                self.sockets.on('com.goabode.timezone', function (data) {
                                    socketMessage('gateway.timezone', data);
                                });
                                self.sockets.on('com.goabode.device.update', function (data) {
                                    socketMessage('device.update', data);
                                });
                                self.sockets.on('com.goabode.devices.refresh', function (data) {
                                    socketMessage('device.update', data);
                                });
                                self.sockets.on('com.goabode.automation', function (data) {
                                    socketMessage('automation', data);
                                });
                                self.sockets.on('com.goabode.billing.plan', function (data) {
                                    socketMessage('billing.plan', data);
                                });
                                // Alarm Events
                                self.sockets.on('com.goabode.alarm.add', function (data) {
                                    socketMessage('alarm.add', data);
                                });
                                self.sockets.on('com.goabode.alarm.del', function (data) {
                                    socketMessage('alarm.del', data);
                                });
                                self.sockets.on('com.goabode.alarm.update', function (data) {
                                    socketMessage('alarm.update', data);
                                });
                                //    com.goabode.gateway.alert.verify
                            });
                        } catch (err) {
                            localLogService.log({type: 'socket connect error', error: err});
                        }
                    });
                });
            },
            disconnect: function () {
                if (this.sockets) {
                    this.sockets.destroy();
                    this.sockets = null;
                }
                if(this.pendingPing){
                    window.clearTimeout(this.pendingPing);
                }
            },
            reset: function () {
                this.disconnect();
                this.connect();
            }
        };
        return socket;

        function startPing() {
            socket.pendingPing = window.setTimeout(function () {
                socket.pendingPing = null;
                if (socket.isConnected()) {
                    socket.sockets.emit('ping', true);
                    startPing();
                }
            }, 25000);
        }

        function socketOpened() {
            localLogService.log({Action: 'socket connected', Background: $rootScope.paused === true});
            $rootScope.$broadcast('sockets.opened');
        }

        function socketReconnecting() {
            if($rootScope.paused){
                socket.disconnect();
            }else{
                localLogService.log({Action: 'socket reconnecting', Background: $rootScope.paused === true});
                userService.getToken().then(function (token) {
                    /*
                     * if the token doesn't exist (e.g. app is logged out), should not
                     * call session API (responding with 403)
                     */
                    if (token) {
                        MessageService.showLoading('Reconnecting application...');
                        userService.userGET().then(function () {
                            api.sockets().then(function (socketsUri) {
                                if(socket.sockets){
                                    socket.sockets.io.uri = socketsUri;
                                }
                            });
                        });
                    } else if (!token) {
                        MessageService.hideLoading();
                        socket.disconnect();
                    }
                });
            }
        }

        function firmwareUpdateDialog() {
            return storageService.setAsync('updating-firmware', 1).then(function () {
                MessageService.show('Firmware Update Available', {
                        'content': 'A software update is available for your abode gateway.' +
                        ' This update contains feature improvements and bug fixes.  We recommend installing this update as soon as possible. ' +
                        ' During the update, your system will be unavailable for approximately 5 minutes.'
                    },
                    [{
                        text: 'Update Now',
                        type: 'button-basic-blue',
                        onTap: function () {
                            confirmUpdate();
                            return true;
                        }
                    }, {
                        text: 'Install Later',
                        type: 'button-clear button-close',
                        onTap: function () {
                            storageService.removeAsync('updating-firmware');
                            return true;
                        }
                    }]
                );
            });

            function confirmUpdate() {
                MessageService.showLoading('Updating Firmware...');
                panelService.firmwareUpdate().then(function (data) {
                    if (!data) {
                        MessageService.hideLoading();
                        storageService.removeAsync('updating-firmware');
                    }
                });
            }

        }


        function socketMessage(type, message) {

            switch (type) {
                case 'gateway.firmware.update':
                    panelService.invalidate();
                    panelService.panelGET(true).then(function (data) {
                        if(data && data.mode.area_1 == 'standby' && data.online == 1 && data.setup_zone_1 === '1' && data.setup_zone_2 === '1' && data.setup_zone_3 === '1'){//jshint ignore:line
                            return storageService.getAsync(['tutorial-show', 'updating-firmware']).then(function (values) {
                                if (values[0] && !values[1]) { // jshint ignore:line
                                    firmwareUpdateDialog();
                                }
                            });
                        }
                    });
                    break;
                case 'gateway.firmware.success':
                    console.log('triggering gateway.online broadcast locally');
                    socketMessage('gateway.online', '');
                    storageService.getAsync('updating-firmware').then(function (updatingFirmware) {
                        if(updatingFirmware == 1){// jshint ignore:line
                            panelService.panelGET().then(function (panel) {
                                if (panel.online == 1) {         // jshint ignore:line
                                    storageService.removeAsync('updating-firmware').then(function () {
                                        MessageService.hideLoading();
                                        MessageService.show('Firmware Updated!', {'content': 'Your gateway\'s firmware has been updated successfully.'}, [{
                                            text: 'Close',
                                            type: 'button-basic-blue'
                                        }]);
                                    });
                                }
                            });
                        }
                    });
                    break;
                case 'gateway.alert.verify':
                    $rootScope.vm.verify = true;
                    $rootScope.$broadcast('sockets.verify');
                    timelineService.invalidate();
                    //$rootScope.$broadcast('sockets.alarm');
                    break;
                case 'gateway.timeline.refresh':
                    timelineService.invalidate();
                    devicesService.invalidate();
                    break;
                case 'gateway.timeline':
                    $log.debug('sockets.message', type, message);
                    $rootScope.$broadcast('sockets.timeline', message);
                    timelineService.invalidate();//TODO just add the current message
                    if (message.event_type === 'Video Capture' || message.event_type === 'Image Capture' || message.event_type === 'Snapshot' || message.event_type === 'Motion Detected') { //jshint ignore:line
                        camerasService.invalidate(); //jshint ignore:line
                    }
                    break;
                case 'gateway.mode':
                    $log.debug('sockets.message', type, message);
                    $rootScope.vm.verify = false;
                    timelineService.invalidate();
                    panelService.invalidate();
                    $rootScope.$broadcast('sockets.mode', message);
                    //$rootScope.$broadcast('sockets.alarm');
                    $rootScope.$evalAsync(function () {
                        $rootScope.vm.timer = 0;
                    });
                    break;
                case 'gateway.online':
                    panelService.invalidate();
                    $rootScope.$broadcast('sockets.alarm');
                    break;
                case 'gateway.timeline.update':
                    $log.debug('sockets.message', type, message);
                    timelineService.invalidate();//TODO just add the current message, should we broadcast another message
                    break;
                //case 'firmware.update':
                //$rootScope.$broadcast('sockets.timeline', message);
                //break;
                case 'gateway.timezone':
                    $rootScope.$broadcast('sockets.opened');
                    break;
                case 'device.update':
                    $log.debug('sockets.message', type, message);
                    devicesService.invalidate();
                    camerasService.invalidate();
                    break;
                case 'automation':
                    $rootScope.$broadcast('sockets.automations');
                    break;
                case 'billing.plan':
                    userService.invalidate();
                    $rootScope.$broadcast('app.plan-upgrade-banner');
                    break;
                case 'alarm.add':
                    $log.debug('sockets.message', type, message);
                    timelineService.invalidateActiveAlarm();
                    $rootScope.$broadcast('sockets.alarm');
                    break;
                case 'alarm.del':
                    $log.debug('sockets.message', type, message);
                    timelineService.invalidateActiveAlarm();
                    $rootScope.$broadcast('sockets.alarm');
                    break;
                case 'alarm.update':
                    $log.debug('sockets.message', type, message);
                    timelineService.invalidateActiveAlarm();
                    $rootScope.$broadcast('sockets.alarm');
                    break;
                default:
                    $log.debug('sockets.message', type, message);
                    break;
            }
            if ($rootScope.vm.invalidate) {
                $rootScope.vm.invalidate();
            }
        }
    }
})();

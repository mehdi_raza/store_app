'use strict';

angular.module('app.config')
    .factory('MessageService', MessageService);

MessageService.$inject = ['$ionicPopup', '$ionicLoading'];
function MessageService($ionicPopup, $ionicLoading) {
    return {
        show: function (title, options, buttons) {
            return $ionicPopup.show({
                templateUrl: 'components/_global/popups/popup-alert.html',
                title: title,
                subTitle: options,
                buttons: buttons
            });
        },
        showError: function (title, message) {
            this.show(title || 'Error', {content: message}, [{
                text: 'OK',
                type: 'button-basic-blue'
            }]);
        },
        showLostInternet:function(callback){
            return this.show('Network Error', {content: 'Error communicating with the server, please check your internet connection, and press Continue.'}, [{
                text: 'Continue',
                type: 'button-basic-blue',
                onTap: function () {
                    if(callback){
                        callback();
                    }
                    return true;
                }
            }]);
        },
        showSuccess: function (text) {
            $ionicLoading.show({
                template: '<i class="icon ion-checkmark" style="font-size: 64px"></i><br />' + text,
                duration: 2000,
            });
        },
        showLoading: function (text) {
            $ionicLoading.show({
                template: text || 'Loading...'
            });
        },
        hideLoading: function () {
            $ionicLoading.hide();
        },
        confirmPhoneDialog : function (title, options, buttons) {
            return $ionicPopup.show({
                templateUrl: 'components/_global/popups/popup-select-phone-numbers.html',
                title: title,
                subTitle: options,
                buttons: buttons
            });
        }
    };
}

'use strict';

(function () {

    angular
        .module('app.config', [
            'ionic',
            'ui.router',
            'restangular',
            'ngAnimate',
            'LocalStorageModule',
            //'ngCordova.plugins.nativeStorage',
            'LocalForageModule',
            'pasvaz.bindonce',
            'ui.utils.masks',
            'ngMaterial'
        ])
        .constant('apiVersion', 'v1/')
        .constant('keyAndroid', '337736643747')
        //.constant('branchKey', 'key_live_hlpOv2orbZwY5ZD2VMt80nmmByp3ne9L')
        .factory('api', apiFactory)
        .factory('queryParam', queryParam)
        .factory('objSize', objSize)
        .factory('$exceptionHandler', [function () {
            return function (exception, cause) {
                window.log({
                     Title: 'Exception!',
                     Message: exception.message || exception,
                     Stack: window.cleanStack(exception.stack || ''),
                     Cause:cause
                 });
            };
        }])
        .config(appConfig)
        .run(runAppConfig);

    appConfig.$inject = ['RestangularProvider', 'localStorageServiceProvider', '$logProvider', '$mdGestureProvider', '$ionicConfigProvider', '$compileProvider'];
    function appConfig(RestangularProvider, localStorageServiceProvider, $logProvider, $mdGestureProvider, $ionicConfigProvider, $compileProvider) {
        RestangularProvider.setDefaultHttpFields({cache: false});
        //RestangularProvider.setDefaultHttpFields({cache: false, withCredentials: true});
        RestangularProvider.setDefaultHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Access-Control-Allow-Credentials': true
        });
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|nestmobile):/);
        localStorageServiceProvider.setPrefix('');
        localStorageServiceProvider.setNotify(true, true);

        $logProvider.debugEnabled(true);

        $ionicConfigProvider.backButton.icon('ion-ios-arrow-left');
        $ionicConfigProvider.navBar.alignTitle('center');
        $ionicConfigProvider.tabs.position('bottom');
        $ionicConfigProvider.tabs.style('standart');

        $mdGestureProvider.skipClickHijack();
        $ionicConfigProvider.backButton.text('');
    }

    runAppConfig.$inject = [
        'Restangular',
        'automationService',
        '$state',
        '$rootScope',
        'storageService',
        'localLogService',
        'userService'
    ];
    function runAppConfig(Restangular, automationService, $state, $rootScope, storageService, localLogService, userService) {

        window.onerror = function (errorMsg, url, lineNumber, column) {//jshint ignore:line
            var errorObj = {
                Title: 'Exception!',
                Message: errorMsg,
                Url: url,
                Line: lineNumber,
                Column: column
            };
            localLogService.log(errorObj);

            console.error('ERROR FOUND: ', errorObj);
        };

        Restangular.setErrorInterceptor(function (response) {

            // While playing video from the camera screen, it gave an error
            // localLogService.log is not a function
            if (localLogService.log) {
                localLogService.log(angular.extend({
                    url: response.config.url,
                    method: response.config.method,
                    status: response.status,
                    statusText: response.statusText
                }, response.config.headers));
            }

             if (response.status === 401 || response.status === 403) {
                // probably have a token, but it was rejected by the server.  let's sign in again
                 userService.invalidate();
                 userService.removeToken().then(function () {
                     if (window.cordova) {
                         automationService.geofencingCancel();
                         navigator.splashscreen.hide();
                     }

                     storageService.getAsync('settings').then(function (settings) {
                         settings.touchId = false;
                         storageService.setAsync('settings', settings).then(function () {
                             if ($rootScope.vm.popup.close) {
                                 $rootScope.vm.popup.close();
                             }
                             if ($state.current.name !== 'register' || $state.current.name !== 'welcome') {
                                 $state.go('signin');
                             }
                         });
                     });
                 });
                 return false; // error handled
             }
            return true;
        });
    }

    apiFactory.$inject = [ 'storageService' ];
    function apiFactory(storageService) {
        return {
            url: function () {
                return storageService.getAsync('stage').then(function (stage) {
                    return  stage ? 'https://zigron.goabode.com/api/' : 'https://my.goabode.com/api/';
                });
            },
            sockets: function () {
                return storageService.getAsync(['stage', 'session']).then(function (values) {
                    if (values[1]) {
                        return values[0] ? 'https://zigron.goabode.com' + '?' + values[1].name + '=' + values[1].id : 'https://my.goabode.com' + '?' + values[1].name + '=' + values[1].id;
                    } else {
                        return '';
                    }
                });
            },
            branchKey: '175983397255009038'
        };
    }

    function queryParam() {
        return {
            value: function (obj) {
                return _.reduce(obj, function (result, value, key) {
                    return (!_.isNull(value) && !_.isUndefined(value)) ? (result += key + '=' + value + '&') : result;
                }, '').slice(0, -1);
            }
        };
    }

    function objSize() {
        return {
            value: function (obj) {
                var size = 0, key;
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        size++;
                    }
                }
                return size;
            }
        };
    }
})();

'use strict';

(function () {
    angular.module('app')
        .filter('orderObjectBy', orderObjectBy)
        .filter('abodeDateFormat', abodeDateFormat)
        .filter('formatCntct', formatCntct)
        .filter('formatDate', formatDate)
        .filter('formatDate2', formatDate2);

    function orderObjectBy() {
        return function (items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                if (Number(a[field])) {
                    a[field] = Number(a[field]);
                }
                if (Number(b[field])) {
                    b[field] = Number(b[field]);
                }
                if (b[field] === a[field]) {
                    return a.id > b.id ? 1 : -1;
                }
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) {
                filtered.reverse();
            }
            return filtered;
        };
    }

    function abodeDateFormat() {
        return function (content) {
            var RstringDate,
                Ldate,
                LdateNow,
                time = (content.verified_time) ? content.verified_time : content.time; //jshint ignore:line

            Ldate = new Date(Date.parse(content.date));
            LdateNow = new Date();
            if (Ldate.getYear() === LdateNow.getYear() &&
                Ldate.getMonth() === LdateNow.getMonth()) {
                if (Ldate.getDate() === LdateNow.getDate()) {
                    RstringDate = 'Today @ ' + time;
                } else if (Ldate.getDate() === LdateNow.getDate() - 1) {
                    RstringDate = 'Yesterday @ ' + time;
                } else {
                    RstringDate = content.date + ' @ ' + time;
                }
            } else {
                RstringDate = content.date + ' @ ' + time;
            }
            return RstringDate;
        };
    }

    formatCntct.$inject = [];
    function formatCntct() {
        return function (content, countryCode) {
            if (countryCode && countryCode === 'US') {
                content = content.replace(/-|\(|\)|\s/g, '');
                if(content.length <= 10) {
                    content = '(' + content.substr(0, 3) + ') ' + content.substr(3, 3) + '-' + content.substr(6, 4);
                } else if (content.length >= 11) {
                    content = content.substr(0,1) + '(' + content.substr(1, 3) + ') ' + content.substr(4, 3) + '-' + content.substr(7, 4);
                }
                return content;
            } else {
                return content;
            }
        };
    }

    formatDate.$inject = [];
    function formatDate() {
        return function (content) {
            var requiredDate = '';
            if(content){
                requiredDate = moment(new Date(content)).format('MM/DD/YYYY') + ' at ' + moment(new Date(content)).format('h:mm A');
            }
            return requiredDate;
        };
    }

    formatDate2.$inject = [];
    function formatDate2() {
        return function (content) {
            var requiredDate = '';
            if(content){
                requiredDate = moment(new Date(content)).format('MM/DD/YYYY h:mm:ss A');
            }
            return requiredDate;
        };
    }

})();

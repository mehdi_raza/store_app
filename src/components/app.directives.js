'use strict';

(function () {
    angular
        .module('app')
        .directive('onLongPress', onLongPress)
        .directive('timelineMedia', timelineMedia)
        .directive('canvasFullscreen', canvasFullscreen)
        .directive('alarmBar', alarmBar)
        .directive('gatewayOffline', gatewayOffline)
        .directive('capitalizeFirst', capitalizeFirst)
        .directive('autocompleteBlur', autocompleteBlur)
        .directive('renderHtml', renderHtml)
        .directive('sbLoad', sbLoad)
        .directive('errImage', errImage)
        .directive('usPhoneNumber', usPhoneNumber);

    onLongPress.$inject = ['$timeout'];
    function onLongPress($timeout) {
        return {
            restrict: 'A',
            link: function ($scope, $elm, $attrs) {
                $scope.safeApply = function(fn) {
                    var phase = this.$root.$$phase;
                    if(phase == '$apply' || phase == '$digest') { // jshint ignore:line
                        if(fn && (typeof(fn) === 'function')) {
                            fn();
                        }
                    } else {
                        this.$apply(fn);
                    }
                };
                $elm.bind('touchstart', function () {
                    // Locally scoped variable that will keep track of the long press
                    $scope.longPress = true;

                    $timeout(function () {
                        if ($scope.longPress) {
                            // If the touchend event hasn't fired,
                            // apply the function given in on the element's on-long-press attribute
                            $scope.safeApply(function () {
                                $scope.$eval($attrs.onLongPress);
                            });
                        }
                    }, 4000);
                });
                $elm.bind('touchend', function () {
                    // Prevent the onLongPress event from firing
                    $scope.longPress = false;
                    // If there is an on-touch-end function attached to this element, apply it
                    if ($attrs.onTouchEnd) {
                        $scope.safeApply(function () {
                            $scope.$eval($attrs.onTouchEnd);
                        });
                    }
                });
            }
        };
    }

    timelineMedia.$inject = ['$sce', 'api', '$mdDialog', 'camerasService'];
    function timelineMedia($sce, api, $mdDialog, camerasService) {
        return {
            restrict: 'A',
            scope: false,
            templateUrl: 'components/_global/timeline-media/timeline-media.template.html',
            link: function (scope) {
                api.url().then(function (url) {
                    var item = scope.$parent.item; //.file_count
                    scope.item = item;
                    if (item.file_del_at === '' || !item.file_del_at) { //jshint ignore:line
                        if (item.device_type_id === '27' || item.device_type_id === '29' || item.device_type_id === '201') { //jshint ignore:line
                            if (item.file_count > '1') { //jshint ignore:line
                                scope.type = 2;
                                var image = url.slice(0, -4) + item.file_path.split('.jpg')[0].slice(0, -3); //jshint ignore:line
                                scope.src = [
                                    {
                                        src: $sce.trustAsResourceUrl(url.slice(0, -4) + item.file_path.split('.jpg')[0].slice(0, -3) + '001.jpg') //jshint ignore:line
                                    }, {
                                        src: $sce.trustAsResourceUrl(url.slice(0, -4) + item.file_path.split('.jpg')[0].slice(0, -3) + '002.jpg') //jshint ignore:line
                                    }, {
                                        src: $sce.trustAsResourceUrl(url.slice(0, -4) + item.file_path.split('.jpg')[0].slice(0, -3) + '003.jpg') //jshint ignore:line
                                    }
                                ];
                            } else {
                                scope.type = 0;
                                scope.src = $sce.trustAsResourceUrl(url.slice(0, -4) + item.file_path); //jshint ignore:line
                            }
                        } else { //jshint ignore:line
                            scope.type = 1;
                            scope.src = item.file_path; //jshint ignore:line
                            scope.poster = $sce.trustAsResourceUrl(url.slice(0, -4) + item.file_path.slice(0, -4) + '.jpg'); //jshint ignore:line
                        }
                    } else {
                        scope.type = 'del';
                        scope.file = item.event_type; //jshint ignore:line
                        scope.date = item.file_del_at; //jshint ignore:line
                    }

                    if (scope.$parent.item.app_deep_link) {                                                 // jshint ignore:line
                        scope.nestDeepLink = url.replace('api/', '') + scope.$parent.item.app_deep_link;    // jshint ignore:line
                    }

                    scope.fullscreen = function (src) {
                        var template = '';
                        if(scope.$parent.item.device_type === 'NestCamera' && scope.$parent.item.event_code === '5301') {           // jshint ignore:line
                            template = '<i class="ion-close close-img" ng-click="fullscreenClose()"></i>' +
                                '<div class="img-bottom-icons">' +
                                '<i class="ion-arrow-expand fullscreen_icon" ng-click="changeOrientation()"></i>' +
                                '</div>' +
                                '<ion-scroll direction="xy" scrollbar-x="true" scrollbar-y="true" zooming="true" min-zoom="1">' +
                                '<img class="loading_img" src="assets/images/cameras/loading.gif">' + '</img>' +
                                '<img class="fullscreen_img" onload="' +
                                "angular.element('.loading_img').hide(); angular.element('.fullscreen_img').show();" +   // jshint ignore:line
                                '" class="fullscreen_img" err-image="assets/images/cameras/noimage.jpg" src="' + src + '">' +
                                '</img></ion-scroll>';
                        } else {
                            /*template = '<ion-scroll direction="xy" scrollbar-x="false" scrollbar-y="false" zooming="true">' +
                                '<canvas style="' + "background: url('assets/images/cameras/loading.gif'); background-size: 80%; background-position: center;" + '" ng-click="fullscreenClose()" canvas-fullscreen="' + src + '"></canvas></ion-scroll>';*/   // jshint ignore:line
                            template = '<i class="ion-close close-img" ng-click="fullscreenClose()"></i>' +
                                '<div class="img-bottom-icons">' +
                                //    '<i class="ion-ios-cloud-download-outline img_download_icon" ng-click="downloadImage(\''+ src +'\')"></i>' +
                                    '<i class="ion-arrow-expand fullscreen_icon" ng-click="changeOrientation()"></i>' +
                                '</div>' +
                                '<ion-scroll direction="xy" scrollbar-x="true" scrollbar-y="true" zooming="true" min-zoom="1">' +
                                '<img class="loading_img" src="assets/images/cameras/loading.gif">' + '</img>' +
                                '<img class="fullscreen_img" onload="' +
                                "angular.element('.loading_img').hide(); angular.element('.fullscreen_img').show();" +   // jshint ignore:line
                                '" class="fullscreen_img" err-image="assets/images/cameras/noimage.jpg" src="' + src + '">'+
                                '</img></ion-scroll>';
                        }
                        $mdDialog.show({
                            template: template,
                            controller: [
                                '$scope', '$q', 'MessageService',
                                function ($scope, $q, MessageService) {
                                    $scope.fullscreenClose = function () {
                                        $q.all([
                                            $mdDialog.hide()
                                        ]).then(function () {
                                            cordova.plugins.screenorientation.setOrientation('portrait');
                                        });
                                    };
                                    $scope.downloadImage = function (src) {
                                        downloadImg(src);
                                    };

                                    $scope.changeOrientation = function () {
                                        cordova.plugins.screenorientation.setOrientation('landscape');
                                        angular.element(document.getElementsByTagName('md-dialog')).addClass('fullscreen-dialog');
                                    };

                                    function downloadImg (src) {
                                        MessageService.showLoading('Downloading image...');
                                        $mdDialog.hide();
                                        var url = encodeURI(src);
                                        if (window.device.platform === 'iOS') {
                                            getDataUri(url, function (str) {
                                                window.CameraRoll.saveToCameraRoll(str, function(success) { //jshint ignore:line
                                                    MessageService.showSuccess('Download Completed');
                                                }, function(err) {
                                                    console.log('Error: ', err);
                                                });
                                            });

                                        } else {
                                            var fileTransfer = new FileTransfer(); //jshint ignore:line
                                            var fileURL = cordova.file.externalRootDirectory + 'abode/abode images/IMG-' + moment(new Date()).format('MMDDYY-hmmss') + '.png';
                                            fileTransfer.download(url, fileURL, function () {
                                                MessageService.showSuccess('Download Completed');
                                                cordova.plugins.MediaScannerPlugin.scanFile(fileURL, function (success) {
                                                    console.log('Success: ', success);
                                                }, function (error) {
                                                    console.log('Error: ', error);
                                                });
                                            }, function (error) {
                                                MessageService.hideLoading();
                                                console.log('Error: ', error);
                                            }, true, null);
                                        }
                                    }

                                    function getDataUri(url, callback) {
                                        var image = new Image();    //jshint ignore:line

                                        image.onload = function () {
                                            var canvas = document.createElement('canvas');
                                            canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
                                            canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

                                            canvas.getContext('2d').drawImage(this, 0, 0);

                                            // Get raw image data
                                            callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

                                            // ... or get as Data URI
                                            callback(canvas.toDataURL('image/png'));
                                        };

                                        image.src = url;
                                    }

                                }
                            ],
                            parent: angular.element(document.getElementById('fullscreen')),
                            clickOutsideToClose: false
                        });

                    };
                    scope.playVideo = function (src) {
                        camerasService.camerasHistoryGET(src).then(function (data) {
                            window.plugins.streamingMedia.playVideo(data.message);
                        });
                    };
                });
            }
        };
    }

    canvasFullscreen.$inject = [];
    function canvasFullscreen() {
        return {
            link: function (scope, elem, attr) {
                var canvas = elem[0];
                var ctx = canvas.getContext('2d');
                var image = document.createElement('img');
                image.onload = function () {
                    var bodyWidth = angular.element(document.body)[0].offsetWidth;
                    ctx.canvas.width = bodyWidth;
                    ctx.canvas.height = image.height / image.width * bodyWidth;
                    //ctx.clearRect(0, 0, canvas.width, canvas.height);
                    //ctx.save();
                    //ctx.translate(0, canvas.height);
                    //ctx.rotate(-90 * Math.PI / 180);
                    ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
                    ctx.restore();
                };
                image.src = attr.canvasFullscreen;
            }
        };
    }

    alarmBar.$inject = [
        'timelineService',
        'panelService',
        '$filter',
        '$ionicHistory',
        '$rootScope'
    ];
    function alarmBar(timelineService, panelService, $filter, $ionicHistory, $rootScope) {
        return {
            restrict: 'A',
            replace: true,
            template: '<a ng-if="show" class="alarm-bar" href="#/details/{{url}}" style="text-decoration: none;" ng-class="{\'verify\': verify}"><div>{{event}}</div></a>',
            controller: [
                '$scope',
                function ($scope) {
                    load();
                    var unregister = $rootScope.$on('sockets.alarm', function () {
                        load();
                    });

                    function load() {

                        timelineService.alarmGET().then(function (alarm) {

                            var alarms = 0;

                            if (Object.keys(alarm).length > 0) {            // alarm exist

                                timelineService.timelineGroupAlarmGET().then(function (data) {
                                    //var alarmsTimeline = $filter('orderObjectBy')(data, 'id', true);
                                    var activeAlarm = data[alarm.start_tid]; //jshint ignore:line
                                    panelService.panelGET().then(function (panel) {

                                        if ((activeAlarm.verified_code === '4000' && panel) || (activeAlarm.verified_by_tid == '0' || activeAlarm.verified_by_tid == '' || activeAlarm.verified_by_tid == null) && !alarms) { //jshint ignore:line
                                            alarms++;
                                            timelineService.timelineGroupTimelineGET(activeAlarm.event_utc, activeAlarm.verified_utc).then(function (data) { //jshint ignore:line
                                                $scope.$evalAsync(function () {
                                                    angular.forEach(data, function (event) {
                                                        if (event.is_alarm === '1') { //jshint ignore:line
                                                            $scope.verify = (activeAlarm.verified_code === '4000'); //jshint ignore:line
                                                            $scope.event = (activeAlarm.verified_code === '4000') ? event.device_name + ' Alarm Verified' : event.device_name + ' ' + event.event_type; //jshint ignore:line
                                                            $scope.url = (activeAlarm.verified_code === '4000') ? activeAlarm.id + '/1' : activeAlarm.id; //jshint ignore:line
                                                        }
                                                    });
                                                    $scope.show = true;
                                                });
                                            });
                                        }

                                    });
                                });
                            } else {
                                if (!alarms) {
                                    $scope.$evalAsync(function () {
                                        $scope.show = false;
                                    });
                                }
                            }
                        });

                    }

                    $scope.$on('$destroy', unregister);
                }
            ]
        };
    }

    gatewayOffline.$inject = ['$rootScope', 'panelService'];
    function gatewayOffline($rootScope, panelService) {
        return {
            restrict: 'A',
            replace: true,
            template: '<div ng-if="panel.online == 0", class="alarm-bar"><div>Gateway Offline</div></div>',
            controller: [
                '$scope',
                function ($scope) {
                    $rootScope.$on('sockets.alarm', function () {
                        $scope.$evalAsync(function () {
                            panelService.panelGET().then(function (panel) {
                                $scope.panel = panel;
                            });
                        });
                    });
                }
            ]
        };
    }

    capitalizeFirst.$inject = ['$parse'];
    function capitalizeFirst($parse) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                var capitalize = function (inputValue) {
                    if (inputValue === undefined) {
                        inputValue = '';
                    }
                    var capitalized = inputValue.charAt(0).toUpperCase() +
                        inputValue.substring(1);
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                };
                modelCtrl.$parsers.push(capitalize);
                capitalize($parse(attrs.ngModel)(scope)); // capitalize initial value
            }
        };
    }

    autocompleteBlur.$inject = ['$timeout'];
    function autocompleteBlur($timeout) {
        return {
            link: function (scope, element, attr) {
                $timeout(function () {
                    element.find('input').bind('blur', function (e) {
                        if (attr.placeholder === 'Country') {
                            window.document.querySelector('#phone').focus();
                        } else {
                            window.document.querySelector('#zip').focus();
                        }
                        e.preventDefault();
                    });
                }, 1000);
            }
        };
    }

    renderHtml.$inject = ['$compile'];
    function renderHtml($compile) {
        return {
            restrict: 'E',
            scope: {
                html: '='
            },
            link: function postLink(scope, element) {
                var newElement = angular.element(scope.html);
                $compile(newElement)(scope);
                element.append(newElement);

                //function appendHtml() {
                //    if (scope.html) {
                //        var newElement = angular.element(scope.html);
                //        $compile(newElement)(scope);
                //        element.append(newElement);
                //    }
                //}
                //
                //scope.$watch(function () {
                //    return scope.html;
                //}, appendHtml);
            }
        };
    }

    errImage.$inject = [];
    function errImage() {
        return {
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src !== attrs.errImage) {
                        attrs.$set('src', attrs.errImage);
                    }
                });
            }
        };
    }

    sbLoad.$inject = ['$timeout'];
    function sbLoad($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                //elem.on('load', function (event) {
                //    setTimeout(function () {
                //        scope.$evalAsync(function () {
                //            fn(scope, {$event: event});
                //        });
                //    }, 1000);
                //});
                element.bind('load', function () {
                    $timeout(function () {
                        if(scope.vm) {
                            if(scope.vm.streamNext) {
                                if(attr.camid) {
                                    scope.vm.streamNext(attr.camid);
                                } else {
                                    scope.vm.streamNext();
                                }
                            }
                        }
                    }, 1000);
                });
                element.bind('error', function () {
                    $timeout(function () {
                        if (scope.vm) {
                            if(scope.vm.streamNextError){
                                scope.vm.streamNextError();
                                //scope.vm.streamNext();
                            }
                        }
                    }, 5000);
                });
            }
        };
    }

    usPhoneNumber.$inject = ['$timeout'];
    function usPhoneNumber($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) { // jshint ignore:line
                element.on('input', function () {
                    scope.$apply(function() {
                        var number = element[0].value;
                        if (number) {
                            var numericVal = number.replace(/-|\+|\*|\#|\.|\,|\;|\/|N|\+|\(|\)|\s/g, '');
                            var nL = numericVal.length;  // Number Length
                            switch (nL) {
                                case 0:
                                    number = '';
                                    break;
                                case 1:
                                    number = '(' + numericVal;
                                    break;
                                case 2:
                                    number = '(' + numericVal;
                                    break;
                                case 3:
                                    number = '(' + numericVal;
                                    break;
                                case 4:
                                    number = '(' + numericVal.substr(0, 3) + ') ' + numericVal.substr(3, 1);
                                    break;
                                case 5:
                                    number = '(' + numericVal.substr(0, 3) + ') ' + numericVal.substr(3, 2);
                                    break;
                                case 6:
                                    number = '(' + numericVal.substr(0, 3) + ') ' + numericVal.substr(3, 3);
                                    break;
                                case 7:
                                    number = '(' + numericVal.substr(0, 3) + ') ' + numericVal.substr(3, 3) + '-' + numericVal.substr(6, 1);
                                    break;
                                case 8:
                                    number = '(' + numericVal.substr(0, 3) + ') ' + numericVal.substr(3, 3) + '-' + numericVal.substr(6, 2);
                                    break;
                                case 9:
                                    number = '(' + numericVal.substr(0, 3) + ') ' + numericVal.substr(3, 3) + '-' + numericVal.substr(6, 3);
                                    break;
                                case 10:
                                    number = '(' + numericVal.substr(0, 3) + ') ' + numericVal.substr(3, 3) + '-' + numericVal.substr(6, 4);
                                    break;
                                case 11:
                                    number = '+' + numericVal.substr(0, 2) + '-' + numericVal.substr(2, 2) + '-' + numericVal.substr(4, 3) + '-' + numericVal.substr(7, 4);
                                    break;
                            }
                        }
                        if (scope.$parent){
                            element[0].value = number;
                            $timeout(function () {
                                element[0].setSelectionRange(element[0].value.length, element[0].value.length);
                            }, 0);
                        }
                    });
                });


            }
        };
    }
})();
